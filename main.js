let tower1 = document.getElementById("tower-1")
let tower2 = document.getElementById("tower-2")
let tower3 = document.getElementById("tower-3")

let disc1 = document.getElementById("disc1")
let disc2 = document.getElementById("disc2")
let disc3 = document.getElementById("disc3")
let disc4 = document.getElementById("disc4")

let currentDisc
let firstClicked // first clicked
let secondClicked // second click
let selected = false

function moveDisc() {
  
    if (secondClicked.childElementCount > 0) {
        const secondDisc = secondClicked.firstElementChild.id.slice(4)
        console.log(secondClicked.firstElementChild.id.slice(4))
        if (currentDisc.id.slice(4) > secondDisc) {
            secondClicked.insertBefore(currentDisc, secondClicked.firstElementChild)
           // secondClicked.appendChild(currentDisc)
        }
    } else {
        secondClicked.appendChild(currentDisc)
    }
    // message box if failed
    firstClicked = undefined
    secondClicked = undefined 
    currentDisc = undefined

    checkWin()
}

function checkWin() {
    if (tower3.childElementCount === 4) {
        alert("You solved the Towers of Hanoi!\nHit refresh to play again!")
        while (tower3.hasChildNodes()) {
            tower3.removeChild(tower3.firstChild);
        }
    }
    else {
        return
    }
}


// After the 1st click (which is the original disc you want to move)
// the second click reassigns the currentDisc !desired

tower1.addEventListener("click", function (event) {
    if(currentDisc === undefined) {currentDisc = event.currentTarget.firstElementChild}
    if(firstClicked === undefined) firstClicked =event.currentTarget
        else secondClicked = event.currentTarget
    if(selected === true) moveDisc()
    selected = !selected
})
// currentDisc = tower1.firstElementChild
//     towerDirect(tower1)
//     selected = !selected

tower2.addEventListener("click", function (event) {
    if(currentDisc === undefined) currentDisc = event.currentTarget.firstElementChild
    if(firstClicked === undefined) firstClicked =event.currentTarget
        else secondClicked = event.currentTarget
    if(selected === true) moveDisc()
    selected = !selected
})

tower3.addEventListener("click", function (event) {
    if(currentDisc === undefined) currentDisc = event.currentTarget.firstElementChild
    if(firstClicked === undefined) firstClicked =event.currentTarget
        else secondClicked = event.currentTarget
    if(selected === true) moveDisc()
    selected = !selected

})
function reset() {
    document.tower1.innerHTML = "<div id='disc1'><div id='disc2'>/,<div id='disc3'><div id='disc4'>";
    document.tower2.innerHTML = " ";
    document.tower3.innerHTML = " ";
        
}
//

